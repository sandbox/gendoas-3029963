/**
 * @file
 */
(function ($, Drupal, drupalSettings, _) {

  "use strict";

  Drupal.sxt_group = {};

  /**
   * 
   */
  var xtgFuncs = {
    hasPermission: function (perm) {
      if (_.isBoolean(perm) ) {
        return perm;
      }
      if (_.isEmpty(perm) ) {
        return false;
      }
      
      var perms = drupalSettings.sxt_group.permissions || {};
      return _.contains(perms, perm);
    }
  };
  $.extend(true, Drupal.sxt_group, xtgFuncs);


})(jQuery, Drupal, drupalSettings, _);
