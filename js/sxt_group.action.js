/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var mainActionsExt = {
    actionRole: function () {
      var args = {
        view: 'XtEditFormView',
//        provider: 'slogxt',
        options: {
          editTarget: 'edit_rolegroup',
//          entityId: 0,
          contentProvider: 'sxt_group'
        }
      };
      _sxt.dialogViewOpen(args);
    }
  };
  
  _sxt.MainActionsView.prototype.actions.sxt_group = mainActionsExt;

})(jQuery, Drupal, drupalSettings);

