<?php

use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\SxtGroup;
use Drupal\sxt_group\SxtGroupInstall;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Implements hook_preprocess_html().
 */
function sxt_group_preprocess_html(&$variables) {
  if (SlogXt::isAdminRoute()) {
    return;   //no changes
  }

  $variables['attributes']['class'][] = 'slogxt-page';
  $page = &$variables['page'];
  $page['#attached']['library'][] = "sxt_group/sxt_group.core";
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sxt_group_form_user_role_form_alter(&$form, FormStateInterface $form_state) {
  \Drupal\sxt_group\XtGrpUserRoleData::formAlter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sxt_group_form_user_admin_roles_form_alter(&$form, FormStateInterface $form_state) {
  \Drupal\sxt_group\XtGrpUserRoleData::formAdminRolesAlter($form, $form_state);
}

/**
 * Implements hook_entity_type_build().
 */
function sxt_group_entity_type_build(array &$entity_types) {
  \Drupal\sxt_group\XtGrpUserRoleData::entityTypeBuild($entity_types);
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function slogxt_user_role_insert(RoleInterface $role) {
  SxtGroup::getGroupSynchronizer()->ensureSxtRoleGroup($role);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function sxt_group_user_role_update(RoleInterface $role) {
  SxtGroup::getGroupSynchronizer()->ensureSxtRoleGroup($role);
}

function sxt_group_group_content_insert(GroupContentInterface $group_content) {
  SxtGroup::getGroupSynchronizer()->ensureSxtRoleMembership($group_content);
}

/**
 * Implements hook_requirements().
 */
function sxt_group_requirements($phase) {
  if ($phase === 'runtime') {
    return SxtGroupInstall::requirements($phase);
  }
}

/**
 * Implements hook_modules_installed().
 */
function sxt_group_modules_installed($modules) {
  SxtGroupInstall::ensureEnforcedGroupTypes();
  SxtGroup::getGroupSynchronizer()->ensureSxtRoleGroupAll();
}

/**
 * Implements hook_rebuild().
 */
function sxt_group_rebuild() {
  SxtGroupInstall::ensureEnforcedGroupTypes();
  SxtGroup::getGroupSynchronizer()->ensureSxtRoleGroupAll();
}

/**
 * Implements hook_group_type_access().
 */
function sxt_group_group_type_access(GroupTypeInterface $group_type, $op, $account) {
  if ((integer) $account->id() === 1) {
    //todo::current::debug::disabled
//    return AccessResult::allowed();
  }
  
  if (in_array($op, ['update', 'delete']) && SxtGroup::isGroupTypeEnforced($group_type)) {
    return AccessResult::forbidden();
  }
  return AccessResult::neutral();
}

/**
 * Implements hook_group_access().
 */
function sxt_group_group_access(GroupInterface $group, $op, $account) {
  if ((integer) $account->id() === 1) {
    //todo::current::debug::disabled
//    return AccessResult::allowed();
  }
  
  $group_type = $group->getGroupType();
  if (in_array($op, ['update', 'delete']) && SxtGroup::isGroupTypeEnforced($group_type)) {
    $role = Role::load($group->label());
    if ($role && SlogXt::isSxtRole($role) //
        && (integer) $role->get('sxt_group_xtrole_group') === (integer) $group->id()) {
      return AccessResult::forbidden();
    }
  }

  return AccessResult::neutral();
}
