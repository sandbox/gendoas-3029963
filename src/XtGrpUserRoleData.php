<?php

/**
 * @file
 * Contains \Drupal\sxt_group\XtGrpUserRoleData.
 */

namespace Drupal\sxt_group;

use Drupal\sxt_group\SxtGroup;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Static functions and callbacks for extended user data.
 * 
 * Data stored in table {users_data}.
 */
class XtGrpUserRoleData {

  /**
   * Add fields to config_export for entity user_role.
   * 
   * @see data for @ConfigEntityType annotation in Role
   * 
   * @param array $entity_types
   *  Array of Drupal\Core\Entity\EntityTypeInterface
   */
  public static function entityTypeBuild(array &$entity_types) {
    if (isset($entity_types['user_role'])) {
      $user_role_type = $entity_types['user_role'];
      $config_export = $user_role_type->get('config_export');
      $config_export[] = 'sxt_group_xtrole_group';
      $user_role_type->set('config_export', $config_export);
    }
  }

  /**
   * Add form fields for sxt_group module.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAlter(&$form, FormStateInterface $form_state) {
    $role = $form_state->getFormObject()->getEntity();
    if (!$role->isNew() && isset($form['slogxtdata']['slogxt_is_xtrole'])) {
      $set_xtrole_group = (integer) $role->get('sxt_group_xtrole_group');
      $has_xtrole_group = SxtGroup::hasSxtRoleGroup($role);
      if ($set_xtrole_group) {
        if ($has_xtrole_group) {
          $description = t('The id of the group created for this role.');
        }
        else {
          $description = t('Group does not exist. Save to fix it.');
        }
      } else {
        $description = t('Group not built for this role. Save to build it.');
      }

      $form['slogxtdata']['sxt_group_xtrole_group'] = [
        '#type' => 'number',
        '#title' => t('SXT-Role Group'),
        '#description' => $description,
        '#default_value' => $set_xtrole_group,
        '#disabled' => TRUE,
      ];
    }
  }

  /**
   * Show column with SXT-Role Group Id
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAdminRolesAlter(&$form, FormStateInterface $form_state) {
    $entities = &$form['entities'];
    $header = $entities['#header'];
    
    $new_header = [];
    foreach ($header as $key => $head) {
      $new_header[$key] = $head;
      if ($key === 'weight') {
        $new_header['xtgroup'] = t('Sxt-Group');
      }
    }
    $entities['#header'] = $new_header;
    
    foreach (Element::children($entities) as $role_id) {
      $role_element = $entities[$role_id];
      $role = Role::load($role_id);
      
      $new_element = [];
      $field_names = Element::children($role_element);
      foreach ($role_element as $key => $value) {
        if (!in_array($key, $field_names)) {
        $new_element[$key] = $value;
        }
      }
      
      $xt_grp_id = '';
      if ((boolean) $role->get('slogxt_is_xtrole')) {
        if (SxtGroup::hasSxtRoleGroup($role)) {
          $xt_grp_id = $role->get('sxt_group_xtrole_group');
        } else {
          $xt_grp_id = '???';
        }        
      }

      foreach ($field_names as $field_name) {
        $field = $role_element[$field_name];
        $new_element[$field_name] = $field;
        if ($field_name === 'weight') {
          $new_element['xtgroup'] = ['#markup' => $xt_grp_id];
        }
      }
      $entities[$role_id] = $new_element;
    }
  }

}
