<?php

/**
 * @file
 * Contains \Drupal\sxt_group\XtGrpRoleSynchronizer.
 */

namespace Drupal\sxt_group;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\SxtGroup;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\user\Entity\Role;
use Drupal\user\UserInterface;
use Drupal\user\RoleInterface;
//use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

//use Drupal\group\Entity\GroupTypeInterface;

/**
 * Synchronizes user roles to group roles.
 */
class XtGrpRoleSynchronizer {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  protected static $sxtRoleGroupTesting = [];

  /**
   * Constructs a new XtGrpRoleSynchronizer.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Make sure that the required group for all (xt) roles are created.
   */
  public function ensureSxtRoleGroupAll() {
    foreach (SlogXt::getSxtRoles() as $role) {
      $this->ensureSxtRoleGroup($role);
    }
  }

  /**
   * Make sure that the required group for a (xt) role is created.
   * 
   * @param RoleInterface $role
   */
  public function ensureSxtRoleGroup(RoleInterface $role) {
    $type_id = SxtGroup::GROUP_TYPE_ID_SXTROLE;
    $role_id = $role->id();
    if (!empty(self::$sxtRoleGroupTesting[$role_id])) {
      return;
    }

    if (SlogXt::isSxtRole($role) && !SxtGroup::hasSxtRoleGroup($role)) {
      try {
        self::$sxtRoleGroupTesting[$role_id] = TRUE;
        $values = [
          'type' => $type_id,
          'label' => $role_id,
        ];
        $group = Group::create($values);
        $group->save();
        $group_id = $group->id();
        $role->set('sxt_group_xtrole_group', $group_id)->save();

        $args = [
          '%type' => $type_id,
          '%role' => $role_id,
          '%group_id' => $group_id,
          '%label' => $group->label(),
        ];
        $msg = t('New created group: %type / %role (%group_id:%label)', $args);
        SlogXt::logger('sxt_group')->notice($msg);
      }
      catch (\RuntimeException $e) {
        $msg = t('Group creation failed:  %type / %role', ['%type' => $type_id, '%role' => $role_id]);
        SlogXt::logger('sxt_group')->error("$msg</br>" . $e->getMessage());
      }

      unset(self::$sxtRoleGroupTesting[$role_id]);
    }
  }

  /**
   * Make sure that the user by group content is member of group's (xt) role.
   * 
   * @param GroupContentInterface $group_content
   */
  public function ensureSxtRoleMembership(GroupContentInterface $group_content) {
    $gc_type_id = $group_content->getGroupContentType()->id();
    if ($gc_type_id !== SxtGroup::getGroupContentTypeId()) {
      return;
    }

    try {
      // check role id: anonymous or authenticated do not assign
      $role_id = $group_content->getGroup()->label(); // group label equals to role id
      if (in_array($role_id, [RoleInterface::AUTHENTICATED_ID, RoleInterface::ANONYMOUS_ID])) {
        return;
      }

      $role = Role::load($role_id);
      if (!$role || !SlogXt::isSxtRole($role)) {
        if ($role) {
          $msg = t('Role is not a xt role:  %role', ['%role' => $role_id]);
          SlogXt::logger('sxt_group')->warning($msg);
        }
        return;
      }

      $user = $group_content->getEntity();
      if (!$user->hasRole($role_id)) {
        $user->addRole($role_id);
        $args = [
          '%type' => $gc_type_id,
          '%group' => $group_content->getGroup()->id(),
          '%role' => $role_id,
          '%user' => $group_content->getEntity()->id(),
        ];
        $msg = t('Membership created:  %type / gid=%group / rid=%role / uid=%user', $args);
        SlogXt::logger('sxt_group')->info($msg);
      }
    }
    catch (\RuntimeException $e) {
      $args = [
        '%type' => $gc_type_id,
        '%group' => $group_content->getGroup()->id(),
        '%role' => $role_id,
        '%user' => $group_content->getEntity()->id(),
      ];
      $msg = t('Membership creation failed:  %type / gid=%group / rid=%role / uid=%user', $args);
      SlogXt::logger('sxt_group')->error("$msg</br>" . $e->getMessage());
    }
  }

}
