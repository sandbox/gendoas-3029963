<?php

/**
 * @file
 * Contains \Drupal\sxt_group\SxtGroup.
 */

namespace Drupal\sxt_group;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\XtUserData;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\User\RoleInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Static functions for sxt_group module
 */
class SxtGroup {

  const GROUP_TYPE_ID_SXTROLE = 'sxtrole';
  const GROUP_CONTENT_TYPE_ID_MEMBERSHIP = 'group_membership';

  /**
   * Cache for there is group content by user/group.
   *
   * @var array 
   */
  protected static $has_group_content = [];

  /**
   * Cache for permissions by user/group.
   *
   * @var array 
   */
  protected static $permissions = [];

  /**
   * Return the xtrole.synchronizer service object.
   * 
   * @return \Drupal\sxt_group\XtGrpRoleSynchronizer
   */
  public static function getGroupSynchronizer() {
    return \Drupal::service('sxt_group.xtrole.synchronizer');
  }

  public static function getGroupContentTypeId($gtype = self::GROUP_TYPE_ID_SXTROLE, $ctype = self::GROUP_CONTENT_TYPE_ID_MEMBERSHIP) {
    return "$gtype-$ctype";
  }

  /**
   * Whether the group type is enforced (protected).
   * 
   * @param string|GroupTypeInterface $type
   * @return boolean
   */
  public static function isGroupTypeEnforced($type) {
    $typeid = ($type instanceof GroupTypeInterface) ? $type->id() : $type;
    return ($typeid === self::GROUP_TYPE_ID_SXTROLE);
  }

  /**
   * Whether for a sxt role there is a group created.
   * 
   * @param RoleInterface $role
   * @return boolean
   */
  public static function hasSxtRoleGroup(RoleInterface $role) {
    $group_id = (integer) $role->get('sxt_group_xtrole_group');
    $type_id = SxtGroup::GROUP_TYPE_ID_SXTROLE;
    $group = Group::load($group_id);
    $group_type = $group ? $group->get('type')->target_id : '';
    if ($group_id > 0 && $group_type === $type_id) {
      return ($group->label() === $role->id());
    }
    return FALSE;
  }

  /**
   * Return group content for a group and user (group_membership)
   * 
   * @param GroupInterface $group
   * @param integer $user_id
   * @return GroupContentInterface or NULL
   */
  public static function getContentByMemberId(GroupInterface $group, $user_id) {
    $gcontents = $group->getContent('group_membership', ['entity_id' => $user_id]);
    return !empty($gcontents) ? array_pop($gcontents) : NULL;
  }

  /**
   * Checks whether a user has the requested group permission.
   * 
   * @param string $permission
   *   The permission string to check.
   * @param AccountInterface $account
   *  Optional - defaults to current user.
   * @param RoleInterface $role
   *  Optional - defaults to the current default role of the current user.
   * @return boolean
   *  Returns FALSE for anonymous user.
   */
  public static function hasPermission($permission, AccountInterface $account = NULL, RoleInterface $role = NULL) {
    $account = $account ?: \Drupal::currentUser();
    // return FALSE for anonymous user
    if ($account->isAnonymous()) {
      return FALSE;
    }

    //
//    $account = User::load('55');  //todo::current::debug::to remove

    if (!$role) {
      $role = XtUserData::getDefaultRole($account, TRUE);
    }
    
    if (!$role || $role->id() === AccountInterface::ANONYMOUS_ROLE) {
      if (SlogXt::getUserHasAdminRole($account)) {
        // all rights for administrators
        return TRUE;
      }
      // no group rights else 
      return FALSE;
    }

    if (self::hasSxtRoleGroup($role)) {
      // ensure user's membership in role
      $user_roles = $account->getRoles();
      $role_id = $role->id();
      if (!in_array($role_id, $user_roles)) {
        $args = [
          '%role' => $role_id,
          '%user' => $account->getUsername(),
          '%user_id' => $account->id(),
        ];
        $msg = t("User not member in role %role: %user (%user_id)", $args);
        SlogXt::logger('sxt_group')->error($msg);
        return FALSE;
      }

      // member of role has view permission for membership
      if ($permission === 'view group_membership content') {
        return TRUE;
      }

      $group_id = (integer) $role->get('sxt_group_xtrole_group');
      $permissions = self::getPermissions($account, $group_id);
      return in_array($permission, $permissions);
    }

    return FALSE;
  }

  /**
   * Whether there is group content for user and group.
   * 
   * @param AccountInterface $account
   * @param integer $group_id
   * @return boolean
   */
  public static function hasGroupContent(AccountInterface $account, $group_id) {
    $uid = $account->id();
    if (!isset(self::$has_group_content[$uid][$group_id])) {
      $group = Group::load($group_id);
      if ($group_content = self::getContentByMemberId($group, $account->id())) {
        self::$has_group_content[$uid][$group_id] = !empty($group_content->group_roles);
      }
      else {
        self::$has_group_content[$uid][$group_id] = FALSE;
      }
    }
    return self::$has_group_content[$uid][$group_id];
  }

  /**
   * Return permissions for current user and current default role.
   * 
   * @return array of strings
   *  return array of permissions, empty array for anonymous user
   */
  public static function getPermissionsCurrent() {
    $account = \Drupal::currentUser();
    // return FALSE for anonymous user
    if ($account->isAnonymous()) {
      return [];
    }

    $role = XtUserData::getDefaultRole($account, TRUE);
    $group_id = (integer) $role->get('sxt_group_xtrole_group');
    return self::getPermissions($account, $group_id);
  }

  /**
   * Return permissions by group content
   * 
   * @param AccountInterface $account
   * @param integer $group_id
   * @return array of permissions
   */
  public static function getPermissions(AccountInterface $account, $group_id) {
    if (!self::hasGroupContent($account, $group_id)) {
      return [];
    }

    $uid = $account->id();
    if (!isset(self::$permissions[$uid][$group_id])) {
      $permissions = \Drupal::service('group_permission.calculator')
          ->calculateMemberPermissions($account)
          ->getMemberPermissions($group_id);
      $ignore = [
        'access content overview',
        'administer group',
        'delete group',
        'edit group',
        'view group',
        'leave group',
        'update own group_membership content',
        'view group_membership content',
      ];
      self::$permissions[$uid][$group_id] = array_values(array_diff($permissions, $ignore));
    }

    return self::$permissions[$uid][$group_id];
  }

}
