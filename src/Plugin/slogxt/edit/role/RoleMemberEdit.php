<?php

/**
 * @file
 * Definition of Drupal\sxt_group\Plugin\slogxt\edit\role\RoleMemberEdit.
 */

namespace Drupal\sxt_group\Plugin\slogxt\edit\role;

/**
 * @SlogxtEdit(
 *   id = "sxt_group_role_member_edit",
 *   bundle = "rolegroup",
 *   title = @Translation("Edit member"),
 *   description = @Translation("Edit a member of the current default role."),
 *   route_name = "sxt_group.edit.member.edit",
 *   skipable = false,
 *   weight = 1
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class RoleMemberEdit extends XtGrpPluginEditBase {

}
