<?php

/**
 * @file
 * Definition of Drupal\sxt_group\Plugin\slogxt\edit\role\RoleMemberAdd.
 */

namespace Drupal\sxt_group\Plugin\slogxt\edit\role;

/**
 * @SlogxtEdit(
 *   id = "sxt_group_role_member_add",
 *   bundle = "rolegroup",
 *   title = @Translation("Add member"),
 *   description = @Translation("Add new member for the current default role."),
 *   route_name = "sxt_group.edit.member.add",
 *   skipable = false,
 *   weight = 2
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class RoleMemberAdd extends XtGrpPluginEditBase {

}
