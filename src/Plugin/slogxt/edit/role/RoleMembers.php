<?php

/**
 * @file
 * Definition of Drupal\sxt_group\Plugin\slogxt\edit\role\RoleMembers.
 */

namespace Drupal\sxt_group\Plugin\slogxt\edit\role;

use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "sxt_group_role_members",
 *   bundle = "rolegroup",
 *   title = @Translation("Show members"),
 *   description = @Translation("Show a list of all members of the current default role."),
 *   route_name = "sxt_group.edit.member.list",
 *   skipable = true,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class RoleMembers extends XtGrpPluginEditBase {

  /**
   * {@inheritdoc}
   */
  protected function preparedPath() {
    return str_replace('{base_entity_id}', $this->baseEntityId, parent::preparedPath());
  }

  protected function access() {
    //todo::current::access
    return SxtGroup::hasPermission('view group_membership content');
  }

}
