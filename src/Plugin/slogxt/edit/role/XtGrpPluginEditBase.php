<?php

/**
 * @file
 * Definition of Drupal\sxt_group\Plugin\slogxt\edit\role\XtGrpPluginEditBase.
 */

namespace Drupal\sxt_group\Plugin\slogxt\edit\role;

use Drupal\sxt_group\SxtGroup;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * 
 */
class XtGrpPluginEditBase extends XtPluginEditBase {

  /**
   * {@inheritdoc}
   */
  protected function preparedPath() {
    return str_replace('{base_entity_id}', $this->baseEntityId, parent::preparedPath());
  }

  protected function access() {
    return SxtGroup::hasPermission('administer members');
  }

}
