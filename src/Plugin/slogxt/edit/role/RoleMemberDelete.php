<?php

/**
 * @file
 * Definition of Drupal\sxt_group\Plugin\slogxt\edit\role\RoleMemberDelete.
 */

namespace Drupal\sxt_group\Plugin\slogxt\edit\role;

/**
 * @SlogxtEdit(
 *   id = "sxt_group_role_member_delete",
 *   bundle = "rolegroup",
 *   title = @Translation("Delete member"),
 *   description = @Translation("Delete a member of the current default role."),
 *   route_name = "sxt_group.edit.member.delete",
 *   skipable = false,
 *   weight = 3
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class RoleMemberDelete extends XtGrpPluginEditBase {

}
