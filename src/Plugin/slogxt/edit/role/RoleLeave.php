<?php

/**
 * @file
 * Definition of Drupal\sxt_group\Plugin\slogxt\edit\role\RoleLeave.
 */

namespace Drupal\sxt_group\Plugin\slogxt\edit\role;

use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "sxt_group_role_member_leave",
 *   bundle = "rolegroup",
 *   title = @Translation("Leave role"),
 *   description = @Translation("Terminate your membership of the current default role."),
 *   route_name = "sxt_group.edit.member.leave",
 *   skipable = false,
 *   weight = 3
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class RoleLeave extends XtGrpPluginEditBase {

  protected function access() {
    //todo::current::access
    return !SxtGroup::hasPermission('administer members');
  }

}
