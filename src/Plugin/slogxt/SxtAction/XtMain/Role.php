<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Plugin\slogxt\SxtAction\XtMain\Role.
 */

namespace Drupal\sxt_group\Plugin\slogxt\SxtAction\XtMain;

use Drupal\sxt_group\SxtGroup;
use Drupal\slogxt\Plugin\SxtActionPluginBase;
use Drupal\slogxt\XtUserData;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_group_role",
 *   title = @Translation("Role"),
 *   menu = "main_dropbutton",
 *   path = "role",
 *   cssClass = "icon-role",
 *   cssItemId = "slogxtmaindropbutton-li-role",
 *   xtProvider = "sxt_group",
 *   weight = -909
 * )
 */
class Role extends SxtActionPluginBase {

  public function getTitle() {
    $current_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    return $current_role->label();
  }

  public function access() {
    return SxtGroup::hasPermission('view group_membership content');
  }

}
