<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Event\SxtGroupEventSubscriber.
 */

namespace Drupal\sxt_group\Event;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\SxtGroup;
use Drupal\slogxt\Event\SlogxtEvents;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Sxt Group event subscriber.
 */
class SxtGroupEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 9999];

    $events[SlogxtEvents::AJAXFORM_RESPONSE_ALTER][] = ['onAjaxFormPreResponse', 10];

    $events[SlogxtEvents::REGISTER_GROUP_PERM_HANDLER][] = ['onRegisterGroupPermHandler', 9999];

    return $events;
  }

  public function onKernelResponse(Event $event) {
    $response = $event->getResponse();
    if ($response instanceof HtmlResponse && !SlogXt::isAdminRoute()) {
      $to_add['drupalSettings']['sxt_group']['permissions'] = SxtGroup::getPermissionsCurrent();
      $response->addAttachments($to_add);
    }
  }

  public function onAjaxFormPreResponse(Event $event) {
    $data = $event->getData();
    $data['form']['#attached']['drupalSettings']['sxt_group'] = [
      'permissions' => SxtGroup::getPermissionsCurrent(),
    ];
  }

  public function onRegisterGroupPermHandler(Event $event) {
    $event->slogxtHandlerClass = 'Drupal\sxt_group\Handler\XtGrpPermissionHandler';
  }

}
