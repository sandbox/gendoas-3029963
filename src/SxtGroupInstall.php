<?php

/**
 * @file
 * Contains \Drupal\sxt_group\SxtGroupInstall.
 */

namespace Drupal\sxt_group;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\SxtGroup;
use Drupal\group\Entity\GroupType;
use Drupal\group\Entity\GroupRole;

/**
 * Static install functions for sxt_group module
 */
class SxtGroupInstall {

  /**
   * Implements hook_requirements().
   */
  public static function requirements() {
    $requirements = [];
    $req_id = 'sxt_group_requirements';
    $req_title = 'SlogSys Group';

    $new_created = self::ensureEnforcedGroupTypes();

    $type_id = SxtGroup::GROUP_TYPE_ID_SXTROLE;
    $group_id = $type_id . '-admin';
    $type = GroupType::load($type_id);
    if (!$type) {
      $requirements[$req_id] = [
        'title' => $req_title,
        'value' => "Group not created: $type_id",
        'severity' => REQUIREMENT_WARNING,
      ];
    }
    else {
      $group_role = GroupRole::load($group_id);
      if (!$group_role) {
        $requirements[$req_id] = [
          'title' => $req_title,
          'value' => "Admin role not created for: $type_id",
          'severity' => REQUIREMENT_WARNING,
        ];
      }
    }

    if (empty($requirements)) {
      $description = empty($new_created) ? '' : 'Some objects has been created: ' . $new_created;
      $requirements[$req_id] = [
        'title' => $req_title,
        'value' => "Available: $type_id, $group_id",
        'description' => $description,
        'severity' => REQUIREMENT_OK,
      ];
    }

    return $requirements;
  }

  /**
   * Ensure group type GROUP_TYPE_ID_SXTROLE and the group role admin for this type.
   */
  public static function ensureEnforcedGroupTypes() {
    $new_created = [];
//            $template_type = self::ensureGroupTypeTemplate();

    $type_id = SxtGroup::GROUP_TYPE_ID_SXTROLE;
    if (!$type = GroupType::load($type_id)) {
      try {
        // add base group type for sxt-roles
        $values = [
          'id' => $type_id,
          'label' => 'Sxt-Role',
          'description' => 'Base group type for SlogXt roles',
          'creator_membership' => TRUE,
          'creator_wizard' => FALSE,
          'creator_roles' => [],
        ];
        GroupType::create($values)->save();
        $type = GroupType::load($type_id);
        $new_created[] = "(type) $type_id";
      }
      catch (\RuntimeException $e) {
        $msg = t('Group type installation failed: %type_id', ['%type_id' => $type_id]);
        SlogXt::logger('sxt_group')->error("$msg</br>" . $e->getMessage());
      }
    }

    $group_role_id = $type_id . '-admin';
    if ($type && !GroupRole::load($group_role_id)) {
      try {
        // add group role for admin
        $group_role = GroupRole::create([
              'id' => $group_role_id,
              'label' => t('Admin'),
              'weight' => 100,
              'group_type' => $type_id,
        ]);
        $group_role->grantAllPermissions()->save();
        // add admin to creator_roles
        $type->set('creator_roles', [$type_id . '-admin'])->save();
        $new_created[] = "(role) $group_role_id";
      }
      catch (\RuntimeException $e) {
        $msg = t('Group roles installation failed: %grole_id', ['%grole_id' => $group_role_id]);
        SlogXt::logger('sxt_group')->error("$msg</br>" . $e->getMessage());
      }
    }

    if (!empty($new_created)) {
      $new_created = implode(', ', $new_created);
      $msg = t('New created objects: %new_created', ['%new_created' => $new_created]);
      SlogXt::logger('sxt_group')->notice($msg);
      return $new_created;
    }

    return '';
  }

}
