<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Routing\XtGrpRoutes.
 */

namespace Drupal\sxt_group\Routing;

use Drupal\slogxt\SlogXt;
use Symfony\Component\Routing\Route;
use Drupal\slogxt\Routing\SlogxtRoutesBase;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class XtGrpRoutes extends SlogxtRoutesBase {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
//    $dummyController = '\Drupal\slogxt\Controller\dummy\DummyCheckboxesController::getContentResult';
//          '_controller' => $dummyController,
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath('sxt_group'), '/');
    $routes = [];

    $raw_data = $this->_rawAjaxEditDefaultRole($baseAjaxPath);

    $defaults = [
      'requirements' => ['_access' => 'TRUE'],
      'options' => [],
      'host' => '',
      'schemes' => [],
      'methods' => [],
      'condition' => null,
    ];

    foreach ($raw_data as $key => $data) {
      $data += $defaults;
      $routes["sxt_group.$key"] = new Route(
          $data['path'], $data['defaults'], $data['requirements'], $data['options'], $data['host'], $data['schemes'], $data['methods'], $data['condition']
      );
    }

    return $routes;
  }

  private function _rawAjaxEditDefaultRole($baseAjaxPath) {
    $baseEditPath = $baseAjaxPath . '/edit_rolegroup';
    $base_entity_id = '{base_entity_id}';   //role_id
    $member_id = '{member_id}';
    $controllerPath = '\Drupal\sxt_group\Handler\XtGrpEdit';

    $raw_data = [
      'edit.actions' => [
        'path' => "/$baseEditPath/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsController::getContentResult",
        ],
      ],
      'edit.member.list' => [
        'path' => "/$baseEditPath/$base_entity_id/manage/list",
        'defaults' => [
          '_controller' => "{$controllerPath}\RoleMembersController::getContentResult",
        ],
      ],
      'edit.member.add' => [
        'path' => "/$baseEditPath/$base_entity_id/manage/add",
        'defaults' => [
          '_controller' => "{$controllerPath}\RoleMemberAddController::getContentResult",
        ],
      ],
      'edit.member.edit' => [
        'path' => "/$baseEditPath/$base_entity_id/manage/$member_id/edit",
        'defaults' => [
          '_controller' => "{$controllerPath}\RoleMemberEditController::getContentResult",
        ],
      ],
      'edit.member.delete' => [
        'path' => "/$baseEditPath/$base_entity_id/manage/$member_id/delete",
        'defaults' => [
          '_controller' => "{$controllerPath}\RoleMemberDeleteController::getContentResult",
        ],
      ],
      'edit.member.leave' => [
        'path' => "/$baseEditPath/$base_entity_id/manage/$member_id/leave",
        'defaults' => [
          '_controller' => "{$controllerPath}\RoleLeaveController::getContentResult",
        ],
      ],
    ];

    return $raw_data;
  }

}
