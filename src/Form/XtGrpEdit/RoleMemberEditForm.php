<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Form\XtGrpEdit\RoleMemberEditForm.
 */

namespace Drupal\sxt_group\Form\XtGrpEdit;

use Drupal\sxt_group\SxtGroup;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Render\Element;

/**
 * Provides a form for editing user's subroles in a role.
 * 
 *  - The role is given by the request.
 *  - The user is to get by 
 */
class RoleMemberEditForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_group_edit_role_member_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $slogxt_data = & $form_state->get('slogxt');
    $this->role_id = $slogxt_data['role_id'];
    $this->group_id = $slogxt_data['group_id'];
    $member_id = $slogxt_data['member_id'];
    $this->user = $user = User::load($member_id);
    $this->role = $role = Role::load($this->role_id);
    $this->group = Group::load($this->group_id);

    $form['base_role'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base role'),
      '#default_value' => $role->label() . " ($this->role_id)",
      '#disabled' => TRUE,
      '#weight' => -990,
    ];

    $allowed = ['base_role', 'entity_id', 'group_roles', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        $form[$field_name]['#access'] = FALSE;
      }
      elseif ($field_name === 'entity_id') {
        $action = (string) t('edit');
        $widget = &$field['widget'];
        $widget['#description'] = t('The name of the user you want to %action membership.', ['%action' => $action]);
        $target_id = &$widget[0]['target_id'];
        $target_id['#description'] = $widget['#description'];
        $target_id['#default_value'] = $user;
        $target_id['#required'] = FALSE;
        $target_id['#disabled'] = TRUE;
      }
      elseif ($field_name === 'group_roles') {
        $field['#access'] = TRUE;
          $field['#attributes']['class'][] = 'slogxt-input-field';

        // load GroupContent (if exists) and set values for group_roles
        $this->group_content = $group_content = SxtGroup::getContentByMemberId($this->group, $user->id());
        if (!empty($group_content)) {
          $group_roles = $group_content->group_roles->referencedEntities();
          if (!empty($group_roles)) {
            $widget = &$field['widget'];
            $widget['#default_value'] = [];
            foreach ($group_roles as $group_role) {
              $widget['#default_value'][] = $group_role->id();
            }
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // prevent validation
    // there is nothing to validate
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $role_id = $this->role->id();
    $user = $this->user;
    $group = $this->group;
    $group_roles = $form_state->getValue('group_roles');
    $group_content = $this->group_content;

    if (empty($group_roles)) {
      if (!empty($group_content)) {
        // delete  existent group content
        $group_content->delete();
      }
    }
    else {
      if (empty($group_content)) {
        // create group content
        $plugin = $group->getGroupType()->getContentPlugin('group_membership');
        $values = [
          'type' => $plugin->getContentTypeConfigId(),
          'gid' => $group->id(),
          'entity_id' => $user->id(),
          'group_roles' => $group_roles,
        ];
        $group_content = \Drupal::entityTypeManager()
            ->getStorage('group_content')
            ->create($values);
        $group_content->save();
      }
      else {
        // override existent group content
        $group_content->group_roles = $group_roles;
        $group_content->save();
      }
    }

    $args = [
      '%user' => $user->label(),
      '%role' => $role_id,
    ];
    drupal_set_message(t('Changes have been saved for %user in role %role', $args));
  }

}
