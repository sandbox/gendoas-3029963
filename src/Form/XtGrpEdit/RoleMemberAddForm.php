<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Form\XtGrpEdit\RoleMemberAddForm.
 */

namespace Drupal\sxt_group\Form\XtGrpEdit;

use Drupal\sxt_group\SxtGroup;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Render\Element;

/**
 * Provides a form for adding user to a role and setting subroles.
 */
class RoleMemberAddForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_group_edit_role_member_add';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $slogxt_data = & $form_state->get('slogxt');
    $this->role_id = $slogxt_data['role_id'];
    $this->group_id = $slogxt_data['group_id'];
    $this->role = Role::load($this->role_id);
    $this->group = Group::load($this->group_id);

    $form['base_role'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base role'),
      '#default_value' => $this->role->label() . " ($this->role_id)",
      '#description' => t('Add user to this role. <br />In addition, you can assign subroles to the user below.'),
      '#disabled' => TRUE,
      '#weight' => -998,
    ];

    $allowed = ['base_role', 'entity_id', 'group_roles', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        $form[$field_name]['#access'] = FALSE;
      }
      elseif (in_array($field_name, ['entity_id', 'group_roles'])) {
          $field['#attributes']['class'][] = 'slogxt-input-field';
        if ($field_name === 'entity_id') {
          // 'CONTAINS' is too slow for a large number of users.
          $target_id = &$field['widget'][0]['target_id'];
          if (is_array($target_id) && !empty($target_id['#selection_settings'])) {
            $target_id['#selection_settings']['match_operator'] = 'STARTS_WITH';
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uid = $form_state->getValue(['entity_id', 0, 'target_id'], 0);
    if (!empty($uid) && $user = User::load($uid)) {
      $this->user = $user;
      $this->group_content = SxtGroup::getContentByMemberId($this->group, $user->id());
      if ($user->hasRole($this->role_id)) {
        $args = [
          '%user' => $user->label(),
          '%role' => $this->role->label(),
          '%hint' => t("Hint: For editing that member use the action 'Edit member'"),
        ];
        $msg = t('User %user is already a member of role %role<br />%hint', $args);
        $form_state->setErrorByName('entity_id]', $msg);
      }
      elseif (empty($this->group_content)) {
        parent::validateForm($form, $form_state);
      }
    }
    else {
      parent::validateForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $role = $this->role;
    $role_id = $role->id();
    $user = $this->user;

    // add user to the role
    $user->addRole($role_id);
    $user->save();
    
    // we create group content only for users with special rights ($group_roles is not empty)
    // so we have three cases to deal with (not for empty $group_roles and $group_content)
    $group_roles = $form_state->getValue('group_roles');
    $group_content = $this->group_content;
    if (empty($group_roles)) {
      if (!empty($group_content)) {
        // delete  existent group content
        $group_content->delete();
      }
    }
    else {
      if (empty($group_content)) {
        // create group content
        parent::save($form, $form_state);
      }
      else {
        // override existent group content
        $group_content->group_roles = $group_roles;
        $group_content->save();
      }
    }

    $args = [
      '%user' => $user->label(),
      '%role' => $role_id,
    ];
    drupal_set_message(t('Membership created for %user in role %role', $args));
  }

}
