<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Form\XtGrpEdit\RoleMemberSelectForm.
 */

namespace Drupal\sxt_group\Form\XtGrpEdit;

use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Render\Element;

/**
 * Provides form selecting a user.
 */
class RoleMemberSelectForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_group_edit_role_member_select';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $slogxt_data = & $form_state->get('slogxt');
    $this->role_id = $slogxt_data['role_id'];
    $this->group_id = $slogxt_data['group_id'];
    $this->role = Role::load($this->role_id);
    $this->group = Group::load($this->group_id);

    $form['base_role'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base role'),
      '#default_value' => $this->role->label() . " ($this->role_id)",
      '#description' => t('Select a user which is member of this role.'),
      '#disabled' => TRUE,
      '#weight' => -998,
    ];

    $allowed = ['base_role', 'entity_id', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        $form[$field_name]['#access'] = FALSE;
      }
      elseif ($field_name === 'entity_id') {
        if ($field_name === 'entity_id') {
          $field['#attributes']['class'][] = 'slogxt-input-field';
          $slogxt_data = $form_state->get('slogxt');
          $action = $slogxt_data['txt_action'];
          $widget = &$field['widget'];
          $widget['#description'] = t('The name of the user you want to %action membership.', ['%action' => $action]);
          $target_id = &$widget[0]['target_id'];
          $target_id['#description'] = $widget['#description'];
          // 'CONTAINS' is too slow for a large number of users.
          if (is_array($target_id) && !empty($target_id['#selection_settings'])) {
            $target_id['#selection_settings']['match_operator'] = 'STARTS_WITH';
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uid = $form_state->getValue(['entity_id', 0, 'target_id'], 0);
    if (!empty($uid) && $user = User::load($uid)) {
      $this->user = $user;
      if (!$user->hasRole($this->role_id)) {
        $args = [
          '%user' => $user->label(),
          '%role' => $this->role->label(),
          '%hint' => t("Hint: For adding a member use the action 'Add member'"),
        ];
        $msg = t('User %user is NOT a member of role %role<br />%hint', $args);
        $form_state->setErrorByName('entity_id]', $msg);
      }
    }
    else {
      parent::validateForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // do not save, is for selection only
    // but add selected user to request
    $slogxt_data = $form_state->get('slogxt');
    if (!empty($slogxt_data['select_key'])) {
      \Drupal::request()->attributes->set($slogxt_data['select_key'], $this->user->id());
    }
  }

}
