<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Form\XtGrpEdit\ActionSelectForm.
 */

namespace Drupal\sxt_group\Form\XtGrpEdit;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;
use Drupal\slogxt\XtUserData;

/**
 * Provides a form for listing actions.
 */
class ActionSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_group_edit_select_action';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    if (empty($baseEntityId)) {
      $default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
      $baseEntityId = $default_role->id();
    }
    return SlogXt::pluginManager('edit')->getActionsData('rolegroup', $baseEntityId);
  }

}
