<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Form\XtGrpEdit\RoleMemberDeleteForm.
 */

namespace Drupal\sxt_group\Form\XtGrpEdit;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for editing user's subroles in a role.
 * 
 *  - The role is given by the request.
 *  - The user is to get by 
 */
class RoleMemberDeleteForm extends RoleMemberEditForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_group_edit_role_member_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $field = & $form['entity_id'];
    $action = (string) t('delete');
    $widget = &$field['widget'];
    $widget['#description'] = t('The name of the user you want to %action membership.', ['%action' => $action]);
    $target_id = &$widget[0]['target_id'];
    $target_id['#description'] = $widget['#description'];

    $form['group_roles']['#disabled'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    try {
      $role_id = $this->role->id();
      $args = [
        '%user' => $this->user->label(),
        '%role' => $role_id,
      ];
      $this->user->removeRole($role_id);
      if ($this->user->hasRole($role_id)) {
        throw new \RuntimeException('Role not removed.');
      }
      if (!empty($this->group_content)) {
        $this->group_content->delete();
      }
    }
    catch (\RuntimeException $e) {
      $msg = t('Membership deletion failed for %user in role %role', $args);
      drupal_set_message($msg, 'error');
      SlogXt::logger('sxt_group')->error("$msg</br>" . $e->getMessage());
      return;
    }

    drupal_set_message(t('Membership deleted for %user in role %role', $args));
  }

}
