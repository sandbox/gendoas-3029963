<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Handler\XtGrpEdit\RoleLeaveController.
 */

namespace Drupal\sxt_group\Handler\XtGrpEdit;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a controller for deleting own membership in a role.
 * 
 *  - The role is given by the request.
 *  - The user is the current user. 
 */
class RoleLeaveController extends RoleMemberDeleteController {

  protected function preFormObjectArg() {
    \Drupal::request()->attributes->set('member_id', \Drupal::currentUser()->id());
    parent::preFormObjectArg();
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    $slogxt_data = & $form_state->get('slogxt');
    $slogxt_data['txt_action'] = (string) t('leave');

    $this->opSave = FALSE;
    $op = \Drupal::request()->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Leave role');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    return $this->getFormTitle();
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    $args = [
      '%role' => $this->role->label(),
      '%warning' => t('WARNING: You cannot undo this action.'),
    ];
    $msg = t('You are about to leave the role %role <br />%warning', $args);
    $form['notice']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    return [
      'command' => 'slogxt::reload',
      'args' => ['reloadImmediately' => TRUE],
    ];
  }

}
