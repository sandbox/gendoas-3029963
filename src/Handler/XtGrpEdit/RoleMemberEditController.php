<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Handler\XtGrpEdit\RoleMemberEditController.
 */

namespace Drupal\sxt_group\Handler\XtGrpEdit;

use Drupal\sxt_group\Handler\XtGrpEdit\RoleMemberHandlerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a controller for editing user's subroles in a role.
 * 
 *  - The role is given by the request.
 *  - The user must be queried first. 
 */
class RoleMemberEditController extends RoleMemberHandlerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    if (empty($this->member_id)) {
      return ($this->getFormObjectArgMemberSelect());
    }

    $group = $this->group;
    $plugin = $group->getGroupType()->getContentPlugin('group_membership');
    $values = [
      'type' => $plugin->getContentTypeConfigId(),
      'gid' => $group->id(),
    ];
    $group_content = $this->entityTypeManager()->getStorage('group_content')->create($values);
    $class = 'Drupal\sxt_group\Form\XtGrpEdit\RoleMemberEditForm';
    $form_arg = $this->classResolver->getInstanceFromDefinition($class);
    $form_arg->setEntity($group_content);
    $form_arg->setModuleHandler(\Drupal::moduleHandler());
    return $form_arg;
  }

  /**
   * Initialize $form_state before building form.
   * 
   * @param FormStateInterface $form_state
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    $slogxt_data = & $form_state->get('slogxt');
    $slogxt_data['txt_action'] = (string) t('edit');
    $slogxt_data['select_key'] = 'member_id';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Edit member');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    if ($this->doMemberSelect) {
      return t('Select');
    }
    else {
      return t('Save changes');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $submitted = $form_state->isSubmitted();
    $hasErrors = $form_state->hasAnyErrors();
    $slogxtData = &$form_state->get('slogxtData');
    $member_id = \Drupal::request()->get('member_id');
    if ($this->doMemberSelect && $submitted && !$hasErrors) {
      if (!empty($member_id)) {
        // return nothing, rebuild form with selected user id
        $this->doMemberSelect = FALSE;
        $this->memberSelected = TRUE;
        $form_state->set('slogxtAjaxRebuild', TRUE);
        // ******************************** return ****
        return [];
      } else {
        // ******************************** return ****
        return $this->buildUrgentMsgContentResult($form, $form_state);
      }
    }
    else {
      $slogxtData['runCommand'] = 'wizardFormEdit';
      $slogxtData['wizardFinalize'] = TRUE;
      if (!empty($this->memberSelected)) {
        $slogxtData['alterTabPath'] = ['{member_id}' => $member_id];
      }
    }
    return parent::buildContentResult($form, $form_state);
  }

}
