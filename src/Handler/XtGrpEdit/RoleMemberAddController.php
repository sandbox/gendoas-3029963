<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Handler\XtGrpEdit\RoleMemberAddController.
 */

namespace Drupal\sxt_group\Handler\XtGrpEdit;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\Handler\XtGrpEdit\RoleMemberHandlerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a controller for adding user to a role and setting subroles.
 * 
 * The role is given by the request.
 */
class RoleMemberAddController extends RoleMemberHandlerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    if (empty($this->group)) {
      return SlogXt::arrangeUrgentUnexpectedErrorForm();
    }

    $group = $this->group;
    $plugin = $group->getGroupType()->getContentPlugin('group_membership');
    $values = [
      'type' => $plugin->getContentTypeConfigId(),
      'gid' => $group->id(),
    ];
    $group_content = $this->entityTypeManager()->getStorage('group_content')->create($values);
    $class = 'Drupal\sxt_group\Form\XtGrpEdit\RoleMemberAddForm';
    $form_arg = $this->classResolver->getInstanceFromDefinition($class);
    $form_arg->setEntity($group_content);
    $form_arg->setModuleHandler(\Drupal::moduleHandler());
    return $form_arg;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Add member');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    return $this->getFormTitle();
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    $slogxtData['wizardFinalize'] = TRUE;
    return parent::buildContentResult($form, $form_state);
  }

}
