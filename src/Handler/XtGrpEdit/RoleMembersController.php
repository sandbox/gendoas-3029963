<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Handler\XtGrpEdit\RoleMembersController.
 */

namespace Drupal\sxt_group\Handler\XtGrpEdit;

use Drupal\sxt_group\SxtGroup;
use Drupal\sxt_group\Handler\XtGrpEdit\RoleMemberHandlerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class RoleMembersController extends RoleMemberHandlerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\XtEmptyInfoForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Members of role %role', ['%role' => $this->role->id()]);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    return '';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    // this is only info, there are no actions
    unset($form['actions']);

    // all members first
    $role_id = $this->role->id();
    $member_count = db_query('SELECT COUNT(roles_target_id) FROM {user__roles} WHERE roles_target_id = :roleid', [':roleid' => $role_id])
        ->fetchField();
    $args = [
      '%role' => $role_id,
      '%all_members' => $member_count,
    ];
    $all_members = t('All members of role %role: %all_members', $args);
    $form['all_members'] = ['#markup' => $all_members];


    // collect infos for group roles
    $memberships = [];
    $gcontents = $this->group->getContent('group_membership');
    foreach ($gcontents as $gcontent) {
      $user = $gcontent->getEntity();
      $uid = $user->id();
      $user_name = $user->label();
      $user_item = "$user_name ($uid)";
      $group_roles = $gcontent->group_roles->getValue();
      foreach ($group_roles as $group_role) {
        $memberships[$group_role['target_id']][$user_name] = $user_item;
      }
    }

    $entityTypeManager = \Drupal::entityTypeManager();
    $definition = $entityTypeManager->getDefinition('group_role');
    $role_storage = $entityTypeManager->getStorage('group_role');
    $query = $role_storage->getQuery()
        ->condition('internal', 0, '=')
        ->condition('group_type', SxtGroup::GROUP_TYPE_ID_SXTROLE, '=')
        ->sort($definition->getKey('weight'));
    $roles = $role_storage->loadMultiple(array_values($query->execute()));
    foreach ($roles as $role_id => $role) {
      if (!empty($memberships[$role_id])) {
        $members = $memberships[$role_id];
        ksort($members);
        $content = implode(', ', $members);
        $form[$role_id] = [
          '#type' => 'details',
          '#title' => $role->label(),
          '#open' => TRUE,
        ];
        $form[$role_id]['users'] = ['#markup' => $content];
      }
    }

    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['wizardFinalize'] = TRUE;
    $slogxtData['runCommand'] = 'wizardFormInfo';
    $slogxtData['isOnLast'] = TRUE;

    return parent::buildContentResult($form, $form_state);
  }

}
