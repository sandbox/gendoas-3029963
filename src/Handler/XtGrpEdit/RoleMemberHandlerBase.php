<?php

/**
 * @file
 * Contains \Drupal\sxt_group\Handler\XtGrpEdit\RoleMemberHandlerBase.
 */

namespace Drupal\sxt_group\Handler\XtGrpEdit;

use Drupal\sxt_group\SxtGroup;
use Drupal\slogxt\SlogXt;
use Drupal\user\Entity\Role;
use Drupal\group\Entity\Group;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a base controller adding, editing, deleting user in a role and change subroles.
 */
abstract class RoleMemberHandlerBase extends XtEditControllerBase {

  protected $doMemberSelect = FALSE;
  protected $memberSelected = FALSE;

// *     "add-form" = "/group/{group}/content/add/{plugin_id}",
// *     "add-page" = "/group/{group}/content/add",
// *     "canonical" = "/group/{group}/content/{group_content}",
// *     "collection" = "/group/{group}/content",
// *     "create-form" = "/group/{group}/content/create/{plugin_id}",
// *     "create-page" = "/group/{group}/content/create",
// *     "delete-form" = "/group/{group}/content/{group_content}/delete",
// *     "edit-form" = "/group/{group}/content/{group_content}/edit"

  protected function preFormObjectArg() {
    $request = \Drupal::request();
    $this->role_id = $role_id = $request->get('base_entity_id');
    $this->member_id = (integer) $request->get('member_id');
    $role = Role::load($role_id);
    if ($role && SxtGroup::hasSxtRoleGroup($role)) {
      $this->role = $role;
      $group_id = (integer) $role->get('sxt_group_xtrole_group');
      if (!$group_id) {
        SxtGroup::getGroupSynchronizer()->ensureSxtRoleGroup($role);
        $group_id = (integer) $role->get('sxt_group_xtrole_group');
      }
      $this->group = Group::load($group_id);
    }

    if (empty($this->group)) {
      $msg = t("Group not found for role: %role", ['%role' => $role_id]);
      SlogXt::logger('sxt_group')->error($msg);
    }
  }

  /**
   * Return form arg for selecting an user.
   * 
   * @return Drupal\sxt_group\Form\XtGrpEdit\RoleMemberSelectForm
   */
  protected function getFormObjectArgMemberSelect() {
    if (empty($this->group)) {
      return SlogXt::arrangeUrgentUnexpectedErrorForm();
    }
    $this->doMemberSelect = TRUE;
    $group = $this->group;
    $plugin = $group->getGroupType()->getContentPlugin('group_membership');
    $values = [
      'type' => $plugin->getContentTypeConfigId(),
      'gid' => $group->id(),
    ];
    $group_content = $this->entityTypeManager()
        ->getStorage('group_content')
        ->create($values);
    $class = 'Drupal\sxt_group\Form\XtGrpEdit\RoleMemberSelectForm';
    $form_arg = $this->classResolver->getInstanceFromDefinition($class);
    $form_arg->setEntity($group_content);
    $form_arg->setModuleHandler(\Drupal::moduleHandler());
    return $form_arg;
  }

  /**
   * {@inheritdoc}
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    if (!empty($this->group)) {
      $slogxt_data = & $form_state->get('slogxt');
      $slogxt_data['role_id'] = $this->role_id;
      $slogxt_data['group_id'] = $this->group->id();
      $slogxt_data['member_id'] = $this->member_id;
    }
  }

}
