<?php

/**
 * @file
 * Definition of \Drupal\sxt_group\Handler\XtGrpPermissionHandler.
 */

namespace Drupal\sxt_group\Handler;

use Drupal\sxt_group\SxtGroup;
use Drupal\slogxt\Handler\XtGrpPermHandlerBase;

class XtGrpPermissionHandler extends XtGrpPermHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return 'sxt_group';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getPermissionsCurrent() {
    return SxtGroup::getPermissionsCurrent();
  }
  
}
